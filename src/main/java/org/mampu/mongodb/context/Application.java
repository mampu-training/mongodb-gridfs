/**
 * 
 */
package org.mampu.mongodb.context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

/**
 * @author shaiful
 * @date   31 Jul 2018
 *
 */
public class Application {

	/**
	 * @param args
	 */
	
			
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		// Upload file using GridFS Driver
		//uploadUsingGridFs();
		
		// Read the file from MongoDb using GridFS Driver
		//readUsingGridFS();
		
		// Upload document using Basic Db Object
		//uploadDocument();
		
		// Read the document using Basic Db Object
		//readDocument();
		
		// Update document
		//updateDocument("800728115712_1503023832082.pdf");
		
		// Delete document
		//deleteDocument("800728115712_1503023832082.pdf");
	}

	private static void uploadUsingGridFs() throws Exception {
		
		System.out.println("Start uploading attachment using GridFS...");
		
		// Initialize MongoDb connection
		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("test");
		DBCollection collection = db.getCollection("attachment");
		
		String folder = "/Users/shaiful/Downloads/";
		
		// Read 1 file
		String fileName = folder + "test.pdf";
		System.out.println("Processing file " + fileName);
		File file = new File(fileName);
		
		// Create attachment namespace
		GridFS gfsPhoto = new GridFS(db, "attachment");
		
		// Get the file from local drive
		GridFSInputFile gfsFile = gfsPhoto.createFile(file);
		gfsFile.setFilename("test.pdf");
		
		// Add the original file name
		gfsFile.put("original_name", "Sijil 1.pdf");
		
		// Save the file into MongoDb
		gfsFile.save();
		
		// Completed
		System.out.println("Completed.");
	}
	
	private static void readUsingGridFS() throws Exception {
		
		System.out.println("Start reading file from MongoDb using GridFS...");
		
		// Initialize MongoDb connection
		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("test");
		DBCollection collection = db.getCollection("attachment");
		
		// The attachment output name
		String folder = "/Users/shaiful/Downloads/test/";
		
		// Create attachment namespace
		GridFS gfsPhoto = new GridFS(db, "attachment");
		
		// Get image file by it's filename
		GridFSDBFile imageForOutput = gfsPhoto.findOne("test.pdf");
		
		// Get the original file name
		String fileName = (String) imageForOutput.get("original_name");
		
		// Save it into a new image file		
		String outputFile = folder + fileName;
		System.out.println("Writing to " + outputFile);
		imageForOutput.writeTo(outputFile);
		
		// Completed
		System.out.println("Completed.");
	}
	
	private static void uploadDocument() throws Exception {
		
		System.out.println("Start uploading attachment using Basic Db Object...");
		
		// Initialize MongoDb connection
		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("mongo_training");
		DBCollection collection = db.getCollection("attachment");
		
		String folder = "/Users/shaiful/Downloads/";
		
		// Read 1 file
		String fileName = folder + "test2.pdf";
		System.out.println("Processing file " + fileName);
		File file = new File(fileName);
		
		// Read the file into byte array
		byte[] bytesArray = new byte[(int)file.length()];
		FileInputStream fis = new FileInputStream(file);
		fis.read(bytesArray);
		fis.close();
		
		// Convert the byte array into BSON format
		Binary bin = new Binary(BsonBinarySubType.BINARY, bytesArray);
		
		// Create the attachment document object
		BasicDBObject bdo = new BasicDBObject();
		bdo.append("_id", "test2.pdf");
		bdo.append("original_name", "Sijil 2.pdf");
		bdo.append("content", bin);
		
		// Save the document
		collection.insert(bdo);
		
		// Completed
		System.out.println("Completed.");
	}
	
	private static void readDocument() throws Exception {
		System.out.println("Start reading document from MongoDb using Basic Db Object...");
		
		// Initialize MongoDb connection
		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("test");
		DBCollection collection = db.getCollection("attachment");
		
		// Get the attachment by id
		DBObject dbo = collection.findOne(new BasicDBObject("_id", "test2.pdf"));
		
		String folder = "/Users/shaiful/Downloads/test/";
		
		// Get the original file name
		String fileName = (String) dbo.get("original_name");
		
		// Get the file content
		byte[] bytesArray = (byte[]) dbo.get("content");	
		
		// Write the content into the original file
		FileOutputStream fout = new FileOutputStream(folder + fileName);		
		fout.write(bytesArray);
        fout.flush();
        fout.close();
        
		// Completed
		System.out.println("Completed.");
	}
	
	private static void updateDocument(String id) throws Exception {
		
		System.out.println("Start updating document from MongoDb using Basic Db Object...");
		
		// Initialize MongoDb connection
		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("mongodb_training");
		DBCollection collection = db.getCollection("attachment");
		
		// Set the new original file name
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.append("$set", new BasicDBObject().append("originalName", "New File Name.pdf"));
		
		// The search query
		BasicDBObject searchQuery = new BasicDBObject().append("_id", id);
		
		// Update the document
		collection.update(searchQuery, newDocument);
		
		// Completed
		System.out.println("Completed.");
	}
	
	private static void deleteDocument(String id) throws Exception {
		
		System.out.println("Start deleting document from MongoDb using Basic Db Object...");
		
		// Initialize MongoDb connection
		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("mongodb_training");
		DBCollection collection = db.getCollection("attachment");
		
		// Set the new original file name
		BasicDBObject document = new BasicDBObject();
		document.put("_id", id);
		
		collection.remove(document);
		
		// Completed
		System.out.println("Completed.");
	}
}
